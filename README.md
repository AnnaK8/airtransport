# Wnioskowanie i optymalizacja w branży lotniczej poprzez analizę danych rezerwacyjnych.

## Wstęp:
Do analizy wybrałam zbiór 'Flight Price Prediction Dataset', który został pobrany z ogólnodostępnego zbioru danych [Flight Price Prediction Dataset](https://www.kaggle.com/datasets/shubhambathwal/flight-price-prediction/data).

Celem jest odpowiedź na poniższe pytania badawcze:
a) Czy cena różni się w zależności od linii lotniczych?
b) Jak cena jest wpływana, gdy bilety są kupowane 1 lub 2 dni przed odlotem?
c) Czy cena biletu zmienia się w zależności od godziny odlotu i przylotu?
d) Jak zmienia się cena w zależności od zmiany miejsca odlotu i miejsca docelowego?
e) Jak różni się cena biletu między klasą ekonomiczną a biznesową?

## Motywacja:
Analiza danych pozwoli na identyfikację czynników wpływających na ceny biletów lotniczych, co może prowadzić do lepszego dostosowania strategii cenowej oraz ofert promocyjnych. Ponadto, badanie zależności między cenami a różnymi parametrami, takimi jak klasa podróży, linie lotnicze czy czas zakupu, umożliwi lepsze zrozumienie preferencji klientów oraz identyfikację obszarów do optymalizacji. Wypracowanie nowych strategii marketingowych oraz usprawnienia procesów operacyjnych na platformie, poprzez odkrycie ukrytych wzorców i trendów w danych. Dodatkowo, wykonanie projektu analizy danych stanowi również okazję do rozwijania umiejętności analitycznych, programistycznych oraz pracy z dużymi zbiorami danych.

## Wnioski ogólne przedstawiłam na końcu notatnika. 
Raport został sporządzony przy pomocy materiałów ze studiów, opracowań znajdujących się na [Kaggle](https://www.kaggle.com/) oraz przy pomocy materiałów naukowych dostępnych na stronach internetowych.
